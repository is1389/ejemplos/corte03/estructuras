Ejemplo básico de definición y declaración de una estructura `struct`. Además se muestra uso de operador punto `.` y flecha `->` para la inicialización y acceso a datos contenidos por la estructura.
