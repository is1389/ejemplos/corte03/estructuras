#include <stdio.h>
#include <stdlib.h>
#define TAM_CENSO 2
#define TAM_NOM 20
#define TAM_FOR 10

typedef enum {
	M,
	F
} Sexo;

typedef struct {
	char nombre[TAM_NOM];
	char apellido[TAM_NOM];
	unsigned edad;
	unsigned id;
	Sexo s;
} Persona;

void ingresarPersona(Persona *p);
void imprimirPersona(const Persona *const p);

int main(void){
	Persona censo[TAM_CENSO];

	for (size_t i = 0; i < TAM_CENSO; i++){
		printf("\nIngresa datos de persona: %zd\n", i + 1);
		ingresarPersona(&censo[i]);
	}

	for (size_t i = 0; i < TAM_CENSO; i++){
		printf("\nImprimiendo datos de persona: %zd\n", i + 1);
		imprimirPersona(&censo[i]);
	}

	exit(EXIT_SUCCESS);
}

void ingresarPersona(Persona *p){
	char formato[TAM_FOR];
	sprintf(formato, "%%%ds", TAM_NOM - 1);

	printf("%s", "Ingresa nombre: ");
	scanf(formato, &(p->nombre));

	printf("%s", "Ingresa apellido: ");
	scanf(formato, &(p->apellido));

	printf("%s", "Ingresa edad: ");
	scanf("%u", &(p->edad));

	printf("%s", "Ingresa ID: ");
	scanf("%u", &(p->id));

	printf("Ingresa sexo (0: M, 1: F): ");
	scanf("%u", &(p->s));
}

void imprimirPersona(const Persona *const p){
	printf("Nombre completo: %s %s\n", p->nombre, p->apellido);
	printf("Edad: %u\n", p->edad);
	printf("ID: %u\n", p->id);
	printf("Sexo: %c\n", (p->s)?'F':'M');
}
